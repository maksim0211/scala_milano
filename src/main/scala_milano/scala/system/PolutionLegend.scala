package system
import org.apache.spark.sql.types.{DoubleType, StringType, IntegerType, StructField, StructType}

object PolutionLegend extends Enumeration {

  val ID, STREET_NAME, LAT, LONGITUDE, TYPE, UOM, TIME_FORMAT = Value

  val structType: StructType = StructType(
    Seq(
      StructField(ID.toString, IntegerType),
      StructField(STREET_NAME.toString, StringType),
      StructField(LAT.toString, DoubleType),
      StructField(LONGITUDE.toString, DoubleType),
      StructField(TYPE.toString, StringType),
      StructField(UOM.toString, StringType),
      StructField(TIME_FORMAT.toString, StringType)
    )
  )
}
case class PolutionLegendCase(id: Int,
                              street_name: String,
                              lat: Double,
                              longitude: Double,
                              `type`: String,
                              uom: String,
                              time_format: String)
