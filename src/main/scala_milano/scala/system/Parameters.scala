package system

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType

object Parameters {
  val POPULATION_DATASET_PATH = ""
  val EXAMPLE_OUTPUT_PATH = "./output/"

  val telecom_delimiter = "\\t"
  val poluition_delimiter = ","

  val path_telecom = "D:\\Res"
  val path_polution_l = "D:\\Res\\pollution\\pollution-legend-mi.csv"
  val path_polution_sq = "D:\\Res\\pollution\\pollution-mi"
  val path_polution_json = "D:\\Res\\milano-grid.geojson"

  val table_telecom = "telecom"
  val table_polution_l = "polution_l"
  val table_polution_sq = "polution_sq"

  private def createTable(name: String, structType: StructType, path: String, delimiter: String )
                         (implicit spark: SparkSession): Unit = {

    spark.read
      .format("com.databricks.spark.csv")
      .options(
        Map(
          "delimiter" -> delimiter,
          "nullValue" -> "\\N",
          "encoding" -> "ISO-8859-1"
        )
      ).schema(structType).csv(path).createOrReplaceTempView(name)
  }

  def initTable(implicit spark: SparkSession): Unit = {
    createTable(Parameters.table_telecom, Telecom.structType, Parameters.path_telecom,Parameters.telecom_delimiter)
    createTable(Parameters.table_polution_l, PolutionLegend.structType, Parameters.path_polution_l,Parameters.poluition_delimiter)
    createTable(Parameters.table_polution_sq, PolutionSquare.structType, Parameters.path_polution_sq,Parameters.poluition_delimiter)
  }
}

