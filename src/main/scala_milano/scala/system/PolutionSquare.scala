package system

import org.apache.spark.sql.types.{DoubleType, IntegerType, StringType, StructField, StructType}

object PolutionSquare extends Enumeration {

  val ID, TIME, MEASUREMENT = Value

  val structType: StructType = StructType(
    Seq(
      StructField(ID.toString, IntegerType),
      StructField(TIME.toString, StringType),
      StructField(MEASUREMENT.toString, DoubleType)
    )
  )
}
case class PolutionSquareCase(id: Int,
                              time: String,
                              measurement: Double)
