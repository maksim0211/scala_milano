package system

import org.apache.spark.sql.types.{DoubleType, IntegerType, LongType, StructField, StructType}

object Telecom extends Enumeration {

  val ID, TIME_INTERVAL, COUNTRY_CODE, SMS_IN, SMS_OUT, CALL_IN, CALL_OUT, TRAFFIC = Value

  val structType: StructType = StructType(
    Seq(
      StructField(ID.toString, IntegerType),
      StructField(TIME_INTERVAL.toString, LongType),
      StructField(COUNTRY_CODE.toString, IntegerType),
      StructField(SMS_IN.toString, DoubleType),
      StructField(SMS_OUT.toString, DoubleType),
      StructField(CALL_IN.toString, DoubleType),
      StructField(CALL_OUT.toString, DoubleType),
      StructField(TRAFFIC.toString, DoubleType)
    )
  )
}
case class TelecomCase(id: Int,
                       time_interval: Long,
                       country_code: Int,
                       sms_in: Double,
                       sms_out: Double,
                       call_in: Double,
                       call_out: Double,
                       traffic: Double)
