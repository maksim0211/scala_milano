package sparkdriver

import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions.{asc, col, desc, lit}
import org.apache.spark.storage.StorageLevel
import stgdata.STG4.getSTG4
import system.Parameters

object SparkDriver extends App {

  val spark = SparkSession.builder()
    .master("local[*]")
    .appName("SparkDriver")
    .config("spark.sql.shuffle.partitions", 2)
    .config("spark.driver.memory", "10g")
    .config("spark.driver.cores", 6)
    .config("spark.memory.offHeap.enabled", true)
    .config("spark.memory.offHeap.size", "13g")
    .config("spark.cores.max", 6)
    .getOrCreate()

  Parameters.initTable(spark)

  val resData = getSTG4().persist(StorageLevel.OFF_HEAP)

  //Функция для создания отчетов топ 5 кварталов по их загрязнению
  def makeReport(dataFrame: DataFrame, filter : String, path : String, isAsc : Boolean = true) = {
    val funcOrder = {
      if (!isAsc) desc("CF")
      else  asc("CF")
    }

    dataFrame
      .filter(filter)
      .orderBy(funcOrder)
      .limit(5)
      .select("ID","SQUARE_INDEX","CF","LAT","LONG")
      .withColumn("valueMeasurement",(col("CF") * 40))
      .withColumn("limitMeasurement",lit(40))
      .write
      .mode(SaveMode.Overwrite)
      .csv(path)
  }

  //Топ 5 «загрязненных» зон
  ///спальные зоны
  makeReport(resData,"SQUARE_INDEX = 1","src/main/scala_milano/results/top_5_polluted_home_zone",false)

  ///рабочие зоны
  makeReport(resData,"SQUARE_INDEX = 2","src/main/scala_milano/results/top_5_polluted_work_zone",false)

  //малонаселенные зоны
  makeReport(resData,"SQUARE_INDEX = 0","src/main/scala_milano/results/top_5_polluted_empty_zone",false)

  ///смешанные зоны
  makeReport(resData,"SQUARE_INDEX = 3","src/main/scala_milano/results/top_5_polluted_mix_zone",false)

  //Топ 5 «чистых»  зон - под словом "чистые" понимаю "наименее загрязненные"
  ///спальные зоны
  makeReport(resData,"SQUARE_INDEX = 1","src/main/scala_milano/results/top_5_clear_home_zone")

  ///рабочие зоны
  makeReport(resData,"SQUARE_INDEX = 2","src/main/scala_milano/results/top_5_clear_work_zone")

  ///малонаселенные зоны
  makeReport(resData,"SQUARE_INDEX = 0","src/main/scala_milano/results/top_5_clear_empty_zone")

  ///смешанные зоны
  makeReport(resData,"SQUARE_INDEX = 3","src/main/scala_milano/results/top_5_clear_mix_zone")

  System.in.read
  spark.stop()
}

