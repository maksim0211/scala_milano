package stgdata

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.{col, udf}
import org.json4s.JValue
import org.json4s.JsonAST.{JDouble, JInt}
import org.json4s.jackson.JsonMethods.parse
import sparkdriver.SparkDriver.spark
import system.Parameters.path_polution_json

object STG2 extends App {

  //Простые функции нахождения примерной середины квадрата для дальнейшего рассчета загрязнения квартала
  def setLAT : UserDefinedFunction = udf((sq_coord: Seq[Double]) => {
    (sq_coord(5)+sq_coord(1))/2.0
  })

  def setLONG : UserDefinedFunction = udf((sq_coord: Seq[Double]) => {
    (sq_coord(0)+sq_coord(4))/2.0
  })

  //"Вытаскиваем" id квартала
  def getCellId(jsonParsed: JValue): List[Int] = {
    for (JInt(cellId) <- jsonParsed \\ "cellId") yield cellId.toInt
  }

  //"Вытаскиваем" лист координат углов квартала
  def getCoord(jsonParsed: JValue): List[Double] = {
    for (JDouble(coordinates) <- jsonParsed \\ "coordinates") yield coordinates
  }

  //Группируем id с соответстующими координатами
  def jsonStringToIdCoord(cellId: List[Int], coordinates: List[Double]) = {
    cellId zip coordinates.grouped(10).toList
  }

  def getSTG2(): DataFrame = {
  val jsonString = parse(scala.io.Source.fromFile(path_polution_json).mkString)
  val jsonList = jsonStringToIdCoord(getCellId(jsonString), getCoord(jsonString))

    //Создаем датафрейм на основе данных, дополнительно находим ширину и долготу центров кварталов
  val miGrid = spark.createDataFrame(jsonList)
    .withColumnRenamed("_1", "ID")
    .withColumnRenamed("_2", "COORDINATES")
    .withColumn("LAT", setLAT(col("COORDINATES")))
    .withColumn("LONG", setLONG(col("COORDINATES")))
    .select("ID","LAT","LONG")

    STG1.getSTG1().join(miGrid, Seq("ID"))
  }
}
