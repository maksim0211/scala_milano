package stgdata

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import sparkdriver.SparkDriver.spark
import system.{Parameters, TelecomCase}

object STG1 extends App {

  //Функция определения типа кваратала
  def getTypeSquare = udf((WORK_HOME_TIME: String, AVG_ACT_WORK_HOME_TIME: Double, AVG_ACT_TOTAL: Double) =>
    if (WORK_HOME_TIME.equals("WORK_TIME")) {
      if (AVG_ACT_WORK_HOME_TIME >= AVG_ACT_TOTAL) 2
      else 0
    }
    else {
      if (AVG_ACT_WORK_HOME_TIME >= AVG_ACT_TOTAL) 1
      else 0
    }
  )

  //Окно по периоду времени
  def byWHT = Window.partitionBy("WORK_HOME_TIME")

  import spark.implicits._

  def getSTG1(telecomSumAct : DataFrame =  spark.table(Parameters.table_telecom)) = {

    //Перевод времени с unixtime в привычный формат и суммирование активностей
    telecomSumAct.as[TelecomCase]
      .withColumn("WORK_HOME_TIME",
        when(from_unixtime(col("TIME_INTERVAL") / 1000, "HH:mm:ss").between("09:00:00", "13:00:00") and
          from_unixtime(col("TIME_INTERVAL") / 1000, "u").between(1, 5), "WORK_TIME")
          .when(from_unixtime(col("TIME_INTERVAL") / 1000, "HH:mm:ss").between("15:00:00", "17:00:00")
            and from_unixtime(col("TIME_INTERVAL") / 1000, "u").between(1, 5), "WORK_TIME").otherwise("HOME_TIME"))
      .withColumn(
        "SUMMARY_ACT",
        when(col("SMS_IN").isNull, 0).otherwise(col("SMS_IN"))
          + when(col("SMS_OUT").isNull, 0).otherwise(col("SMS_OUT"))
          + when(col("CALL_IN").isNull, 0).otherwise(col("CALL_IN"))
          + when(col("CALL_OUT").isNull, 0).otherwise(col("CALL_OUT"))
          + when(col("TRAFFIC").isNull, 0).otherwise(col("TRAFFIC")))
      //Нахождение среднего трафика в рабочее время и в нерабочее время в городе, а также в каждом квартале => нахождение типа квартала
      .withColumn("AVG_ACT_TOTAL", avg("SUMMARY_ACT").over(byWHT))
      .select("ID", "WORK_HOME_TIME", "SUMMARY_ACT", "AVG_ACT_TOTAL")
      .groupBy("ID", "WORK_HOME_TIME", "AVG_ACT_TOTAL")
      .agg(
        avg("SUMMARY_ACT") as "AVG_ACT_WORK_HOME_TIME"
      )
      .withColumn("INDEX", getTypeSquare(col("WORK_HOME_TIME"), col("AVG_ACT_WORK_HOME_TIME"), col("AVG_ACT_TOTAL")))
      .select("ID", "INDEX")
      .groupBy("ID")
      .sum("INDEX")
      .withColumnRenamed("sum(INDEX)", "SQUARE_INDEX")
  }
}
