package stgdata

import org.apache.spark.sql.DataFrame
import sparkdriver.SparkDriver.spark
import system.{Parameters, PolutionLegendCase, PolutionSquareCase}

object STG3 extends App {

  import  spark.implicits._

  def getSTG3(): DataFrame = {

    //Сопоставляем данные о загрязнении в городе от датчиков с конкретными датчиками
    spark.table(Parameters.table_polution_l).as[PolutionLegendCase]
      .join(
        spark.table(Parameters.table_polution_sq).as[PolutionSquareCase]
        , Seq("ID")
      )
      .withColumnRenamed("ID","SENSOR_ID")
  }
}
