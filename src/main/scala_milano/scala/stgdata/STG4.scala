package stgdata
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.{col, udf}
import stgdata.STG2.getSTG2

object STG4 extends App {

  def approxXY: UserDefinedFunction = udf((x: Double, y: Double) => {
    val xx = -137219 + 6037.1 * x - 66.4011 * x * x
    val yy = -59.36282800 * y * y + 1088.04405937 * y - 4983.81368452
    (xx + yy) / 2.0
  })


def getSTG4() : DataFrame = {
  getSTG2()
    .withColumn("CF", approxXY(col("LAT"),col("LONG")))
    .select("ID","SQUARE_INDEX","CF","LAT","LONG")
  }
}